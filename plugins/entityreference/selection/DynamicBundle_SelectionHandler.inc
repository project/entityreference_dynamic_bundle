<?php

$plugin = array(
  'title' => t("Dynamic: Select the bundle according to the instance"),
  'class' => 'DynamicBundle_SelectionHandler',
  'weight' => 100,
);
